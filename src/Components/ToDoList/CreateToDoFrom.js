import { Button, Box, TextField } from "@material-ui/core";
import { useState } from "react";
import { generateID } from "../../util/generateID";
import "./CreateToDoFrom.css";

const CreateToDoFrom = ({ props, onSubmit }) => {
  const [form, setForm] = useState({
    ID: -1,
    Title: "",
    Text: "",
    IsDeleted: false,
    CreatedDate: new Date(),
  });
  const handleForm = (event) => {
    const { id, value } = event.target;
    setForm((prevState) => ({ ...prevState, [id]: value }));
  };

  const onSubmitForm = () => {
    if (form.Title && form.Text) {
      form.ID = generateID();
      onSubmit(form);
    }
  };
  return (
    <div className="to-do-form">
      <Box
        component="form"
        sx={{
          "& .MuiTextField-root": { m: 1, width: "25ch" },
        }}
        noValidate
        autoComplete="off"
      >
        {Object.keys(props)
          .filter((item) => item !== "IsDeleted" && item != "CreatedDate")
          .map((item) => {
            return (
              <div className="to-do-form__input">
                <TextField
                  id={item}
                  label={`Type ${item}`}
                  placeholder={item}
                  onChange={handleForm}
                />
                {item === "Title" && !form.Title && (
                  <p className="text-error">please enter type title</p>
                )}
                {item === "Text" && !form.Text && (
                  <p className="text-error">please enter type text</p>
                )}
              </div>
            );
          })}
        <Button onClick={onSubmitForm} variant="outlined">
          Add
        </Button>
      </Box>
    </div>
  );
};

export default CreateToDoFrom;
