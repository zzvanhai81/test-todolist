import React, { useEffect } from "react";
import { Grid, Button, Typography, Box } from "@material-ui/core";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import TodoItem from "./TodoItem";
import CreateToDoFrom from "./CreateToDoFrom";
import { setToDoList, setStatusToDoItem } from "../../redux/reducers/coreSlice";
import "./TodoList.css";

const TodoList = () => {
  const [createNewTask, setCreateNewTask] = useState(false);
  const [toDoListIDDone, setToDoListIDDone] = useState([]);
  const [sortTodo, setSortTodo] = useState([]);
  const { ToDo, ToDoList } = useSelector((state) => state.core);
  const dispatch = useDispatch();

  useEffect(() => {
    setSortTodo([...ToDoList].sort((a, b) => b.CreatedDate - a.CreatedDate));
  }, [ToDoList]);

  const onItemChecked = (event, item) => {
    // TODO
    if (item.IsDeleted) {
      let index = ToDoList.findIndex((todo) => todo.ID === item.ID);
      dispatch(
        setStatusToDoItem({
          index,
          isDeleted: false,
        })
      );
    } else {
      let index = toDoListIDDone.findIndex((ID) => ID === item.ID);
      if (index === -1) {
        toDoListIDDone.push(item.ID);
        setToDoListIDDone([...toDoListIDDone]);
      } else {
        toDoListIDDone.splice(index, 1);
        setToDoListIDDone([...toDoListIDDone]);
      }
    }
  };

  const onSubmitNewTask = (newTodo) => {
    dispatch(setToDoList(newTodo));
  };

  const onRemoveItem = (item) => {
    var answer = window.confirm("Are you sure you want to delete this entry?");
    if (answer) {
      let index = ToDoList.findIndex((todo) => todo.ID === item.ID);
      dispatch(
        setStatusToDoItem({
          index,
          isDeleted: true,
        })
      );
    }
  };

  return (
    <div className="to-do-list">
      <Grid container>
        <Grid xs={4}>
          <Box>
            <Typography>Hello Chandrika :)</Typography>
            <Typography>Here is your to do list</Typography>
            {!createNewTask ? (
              <Button
                onClick={() => setCreateNewTask(true)}
                variant="contained"
              >
                Create new task
              </Button>
            ) : null}
            {createNewTask ? (
              <CreateToDoFrom props={{ ...ToDo }} onSubmit={onSubmitNewTask} />
            ) : null}
            {createNewTask ? (
              <Button onClick={() => setCreateNewTask(false)} variant="text">
                Cancel
              </Button>
            ) : null}
          </Box>
        </Grid>
        <Grid xs={8}>
          <Typography>To Do List</Typography>
          {sortTodo
            .filter((td) => {
              return td.IsDeleted === false;
            })
            .map((item, idx) => {
              return (
                <TodoItem
                  props={item}
                  onItemChecked={(e) => onItemChecked(e, item)}
                  toDoListIDDone={toDoListIDDone}
                  onRemoveItem={onRemoveItem}
                />
              );
            })}
          <Typography>Archive List</Typography>
          {sortTodo
            .filter((td) => {
              return td.IsDeleted === true;
            })
            .map((item, idx) => {
              return (
                <TodoItem
                  props={item}
                  onItemChecked={(e) => onItemChecked(e, item)}
                />
              );
            })}
        </Grid>
      </Grid>
    </div>
  );
};

export default TodoList;
