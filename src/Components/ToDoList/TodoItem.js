import { Box, Checkbox, Typography } from "@material-ui/core";
import "./TodoItem.css";

const TodoItem = ({ props, onItemChecked, toDoListIDDone, onRemoveItem }) => {
  let isToDoListIDDone =
    toDoListIDDone && toDoListIDDone.findIndex((ID) => ID === props.ID) !== -1;
  return (
    <div className={`to-do-item ${isToDoListIDDone ? "active" : ""}`}>
      <Box>
        <div className="to-do-item__content">
          <Checkbox
            onChange={(e) => onItemChecked(e, props)}
            checked={!props.IsDeleted ? isToDoListIDDone : false}
          />
          <Box>
            <Typography>{props.Title}</Typography>
            <Typography>{props.Text}</Typography>
          </Box>
        </div>
        {!props.IsDeleted && (
          <img
            src={require("../../images/delete.svg").default}
            alt="img"
            className="img-remove"
            onClick={() => onRemoveItem(props)}
          />
        )}
      </Box>
    </div>
  );
};

export default TodoItem;
